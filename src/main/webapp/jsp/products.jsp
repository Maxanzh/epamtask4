<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" language="java"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
    <link rel="stylesheet" type="text/css" href="css/main-style.css"/>
    <script type="text/javascript" src="js/validation.js"></script>
    <title>Products page</title>
</head>
<body>
    <nested:form action="UpdateProduct.do" onsubmit="return checkvalue(this)">
        <c:set var="categIndex" value="${productForm.categoryIndex}"/>
        <c:set var="subcategIndex" value="${productForm.subcategoryIndex}"/>
        <c:set var="categoryName" 
                value="${productForm.catalog.rootElement.children[categIndex].attributes[0].value}"/>
        <c:set var="subcategoryName"
                value="${productForm.catalog.rootElement.children[categIndex].children[subcategIndex].attributes[0].value}"/>

        <input type="hidden" name="method" value="update"/>				

        <table>
        <c:if test="${not(validationMessage eq null)}">
            <tr>
                    <td colspan="7">${validationMessage}</td>
            </tr>
        </c:if>
        <tr>
            <td>Product name</td>
            <td>Producer</td>
            <td>Model</td>
            <td>Date of issue</td>
            <td>Color</td>
            <td>Price</td>
          
        </tr>
            <nested:nest property="catalog.rootElement.children[${categIndex}].children[${subcategIndex}]">
                <nested:iterate property="children" indexId="i">
                    <tr>
                        <td>
                            <nested:text property="attributes[0].value"  styleId="productName"/>
                        </td>					
                        <td>
                            <nested:text property="children[0].text" styleId="producer"/>
                        </td>
                        <td>
                            <nested:text property="children[1].text" maxlength="5"
                            styleId="model"/>
                        </td>
                        <td>
                            <nested:text property="children[2].text" maxlength="10"
                             styleId="dateOfIssue"/>            
                        </td>
                        <td>
                            <nested:text property="children[3].text" styleId="color" />
                        </td>
                        <c:choose>
                            <c:when test="${productForm.catalog.rootElement.children[categIndex].children[subcategIndex].children[i].children[4].name eq 'price'}">
                                <td>
                                    <nested:text property="children[4].text" styleId="price"/>
                                </td>
                            </c:when>
                            <c:otherwise>
                                <td> <label>Not in stock</label> </td>                            
                            </c:otherwise>
                        </c:choose>
                    </tr>			

                </nested:iterate>
            </nested:nest>
		
            <tr>
                <td>
                    <input type="button" value="back"
                        onclick="location.href='Navigate.do?pageType=SUBCATEGORIES&categoryIndex=${categIndex}&subcategoryIndex=${subcategIndex}'"/>
                </td>
                <td>
                     <input type="button" value="add"
                                onclick="location.href='AddPage.do?categoryIndex=${categIndex}&subcategoryIndex=${subcategIndex}&categoryName=${categoryName}&subcategoryName=${subcategoryName}'"/>
                </td>
                <td>
                    <nested:submit value="save"/>
                </td>
            </tr>
		        </table>
    </nested:form>
	
	
</body>
</html>

