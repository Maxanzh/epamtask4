<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" language="java"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<title>Subcategories List</title>
<link rel="stylesheet" type="text/css" href="css/main-style.css"/>
</head>
<body>
    <nested:form action="Navigate.do">
        <c:set var="categIndex" value="${productForm.categoryIndex}"/>
        <table>
            <nested:nest property="catalog.rootElement.children[${categIndex}]">
                <nested:iterate property="children" indexId="i">
                <c:set var="count" value="0"/>

                <!-- count up products -->
                <nested:iterate property="children">
                        <c:set var="count" value="${count + 1}"/>
                </nested:iterate>
                    <tr>
                        <td>
                            <a href="Navigate.do?categoryIndex=${categIndex}&subcategoryIndex=${i}&pageType=PRODUCTS">
                                <nested:write property="attributes[0].value"/>
                            </a>
                        </td>
                        <td>
                            ${count}					
                        </td>
                    </tr>
                </nested:iterate>
            </nested:nest>
            <tr>
                <td>
                    <input type="button" value="back" onclick="location.href='Navigate.do?pageType=CATEGORIES'"/>                  
                </td>
            </tr>
        </table>
    </nested:form>
	
</body>
</html>

