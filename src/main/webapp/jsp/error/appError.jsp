
<%@page contentType="text/html" pageEncoding="UTF-8" isErrorPage="true"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Application Error</title>
    </head>
    <body>
        <div class="content">
            <h2>Application Error</h2> 
            <html:errors/> </br>
            <html:link action="/Navigate.do?pageType=CATEGORIES">
               Category Page
            </html:link>
        </div>
    </body>
</html>



