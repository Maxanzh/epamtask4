<%-- 
    Document   : categories
    Created on : Apr 3, 2014, 5:11:29 PM
    Author     : Maxim_Zhupinsky
--%>

<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" language="java"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/main-style.css"/>
        <title>Category List</title>
    </head>
    <body>
        
            <table>
                <nested:form action="Navigate.do">
                <nested:nest property="catalog.rootElement">
                    <nested:iterate property="children" indexId="i">
                    <c:set var="count" value="0"/>

                    <!-- count up products -->
                    <nested:iterate property="children">
                            <nested:iterate property="children">
                                    <c:set var="count" value="${count + 1}"/>
                            </nested:iterate>
                    </nested:iterate>
                        <tr>
                            <td>
                                
                                <a href="Navigate.do?categoryIndex=${i}&pageType=SUBCATEGORIES">
                                    <nested:write property="attributes[0].value"/>
              
                                </a>
                            </td>
                            <td>
                                ${count}					
                            </td>
                        </tr>
                    </nested:iterate>
                </nested:nest>
                </nested:form>
            </table>
    </body>
</html>
