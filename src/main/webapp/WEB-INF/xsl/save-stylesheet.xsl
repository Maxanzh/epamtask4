<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:nms="http://www.epam.com/catalogXMLSchema"
		xmlns:validator="xalan://com.epam.testapp.logic.xslt.validator.ProductValidator"
		exclude-result-prefixes="validator">

    <xsl:import href="write-stylesheet.xsl"/>
    <xsl:include href="add-page-content.xsl"/>
    
    <xsl:output method="html" indent="yes"/>

    <xsl:param name="xmlFilepath"/>
    <xsl:param name="mission"/>
    <xsl:param name="categoryName"/>
    <xsl:param name="subcategoryName"/>
    <xsl:param name="categIndex"/>
    <xsl:param name="subcategIndex"/>
    <xsl:param name="productName"/>
    <xsl:param name="producer"/>
    <xsl:param name="model"/>
    <xsl:param name="dateOfIssue"/>
    <xsl:param name="color"/>
    <xsl:param name="price"/>
    <xsl:param name="validator"/>

    
	<xsl:template match="/">
            <!-- validate and write the product.  -->
            <xsl:if test="$mission = 'full'">
                <xsl:if test="validator:validateProduct($validator,$productName, $producer, $model, $dateOfIssue, $color, $price)"/>
                <xsl:choose>
                    <xsl:when test="false() = validator:isValidationFlag($validator)">
                         <xsl:call-template name="addPage"/>           
                    </xsl:when>
                    <xsl:otherwise>
                             <xsl:apply-imports />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:if>
			
            <!-- if file has been rewrote it must be parsed again 
                but validation is not needed, so call just writing
                part of logic -->
            <xsl:if test="'just_write' = $mission">
                    <xsl:for-each select="/catalog">
                            <xsl:apply-imports/>
                    </xsl:for-each>
            </xsl:if>
	</xsl:template>
        
</xsl:stylesheet>

        