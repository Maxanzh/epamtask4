<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
			xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                        xmlns:nms="http://www.epam.com/catalogXMLSchema"
                         xmlns:validator="xalan://com.epam.testapp.logic.xslt.validator.ProductValidator"
			>

        <xsl:include href="add-page-content.xsl"/>	
	
	<xsl:output method="html" indent="yes"/>

	<xsl:param name="categIndex"/>
	<xsl:param name="subcategIndex"/>
	<xsl:param name="categoryName"/>
	<xsl:param name="subcategoryName"/>
	<xsl:param name="productName" />
	<xsl:param name="producer" />
	<xsl:param name="model" />
	<xsl:param name="dateOfIssue"/>
	<xsl:param name="color" />
	<xsl:param name="price" />
        <xsl:param name="validator"/>
	
	
	<xsl:template match="/">
		<xsl:call-template name="addPage"/>
	</xsl:template>
        
 </xsl:stylesheet>       
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
<!--        
	<xsl:template match="/">
	<script type="text/javascript" src="js/validation.js"></script>
	<html>
    	<head>
            <link rel="stylesheet" type="text/css" href="css/main-style.css"/>
            <title>Products</title>
        </head>
	
    	<body>
    	<table>
    	<form action="SaveProduct.do" method="post">
            <input type="hidden" name="command" value="save"/>
    	    <input type="hidden" name="categoryName" value="{$categoryName}"/>
            <input type="hidden" name="subcategoryName" value="{$subcategoryName}"/>
    		<tr>
                    <td>Product Name: </td>
                    <td><input type="text" name="productName"/></td>
                    <td class="prompt">sdfsdff</td>
    		</tr>
    		<tr>
                    <td>Producer: </td>
                    <td><input type="text" name="producer"/></td>
                    <td class="prompt">sdfsdff</td>
    		</tr>
    		<tr>
                    <td>Model: </td>
                    <td><input type="text" name="model"/></td>
                    <td class="prompt"><xsl:value-of select="message:getStr('appResource.pattern.prompt.model')"/></td>
    		</tr>
    		<tr>
                    <td>Date of issue: </td>
                    <td><input type="text" name="dateOfIssue"/></td>
                    <td class="prompt"><xsl:value-of select="message:getStr('appResource.pattern.prompt.date')"/></td>
    		</tr>
    		<tr>
                    <td>Color: </td>
                    <td><input type="text" name="color"/></td>
                    <td class="prompt"><xsl:value-of select="message:getStr('appResource.pattern.prompt.color')"/></td>
    		</tr>
    		<tr>
                    <td>Price: </td>
                    <td><input type="text" name="price"/></td>
                    <td class="prompt"><xsl:value-of select="message:getStr('appResource.pattern.prompt.price')"/></td>
    		</tr>
    		<tr>
                    <td>
                        <input type="button" value="back"
                        onclick="location.href='Navigate.do?page_type=SUBCATEGORIES&categoryIndex=${categIndex}&subcategoryIndex=${subcategIndex}'"/>
                    </td>
                    <td>
                        <input type="submit" value="save"></input>
                    </td>
    		</tr>
    	</form>
    	</table>
		</body>
   	</html>
	</xsl:template>
	
</xsl:stylesheet>-->