<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
			xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                        xmlns:nms="http://www.epam.com/catalogXMLSchema"
                        xmlns:validator="xalan://com.epam.testapp.logic.xslt.validator.ProductValidator"
                        exclude-result-prefixes="validator">
    
    <xsl:output method="html" indent="yes"/>	

    <xsl:template name="addPage">
        <html>
        <head>
            <link rel="stylesheet" type="text/css" href="css/main-style.css"/>
            <title>Products</title>
        </head>

        <body>
        <table>
        <form action="SaveProduct.do" method="post">
            <input type="hidden" name="command" value="save"/>
            <input type="hidden" name="categoryName" value="{$categoryName}"/>
            <input type="hidden" name="subcategoryName" value="{$subcategoryName}"/>
                <tr>
                    <td>Product Name: </td>
                    <td><input type="text" name="productName" value="{$productName}"/></td>
                    <td class="prompt"><xsl:value-of select="validator:getError($validator,'productName')"/></td>
                </tr>
                <tr>
                    <td>Producer: </td>
                    <td><input type="text" name="producer" value="{$producer}"/></td>
                    <td class="prompt"><xsl:value-of select="validator:getError($validator,'producer')"/></td>
                </tr>
                <tr>
                    <td>Model: </td>
                    <td><input type="text" name="model" value="{$model}"/></td>
                    <td class="prompt"><xsl:value-of select="validator:getError($validator,'model')"/></td>
                </tr>
                <tr>
                    <td>Date of issue: </td>
                    <td><input type="text" name="dateOfIssue" value="{$dateOfIssue}"/></td>
                   <td class="prompt"><xsl:value-of select="validator:getError($validator,'dateOfIssue')"/></td>
                </tr>
                <tr>
                    <td>Color: </td>
                    <td><input type="text" name="color" value="{$color}"/></td>
                    <td class="prompt"><xsl:value-of select="validator:getError($validator,'color')"/></td>
                </tr>
                <tr>
                    <td>Price: </td>
                    <td><input type="text" name="price" value="{$price}"/></td>
                    <td class="prompt"><xsl:value-of select="validator:getError($validator,'price')"/></td>
                </tr>
                <tr>
                    <td>
                        <input type="button" value="back"
                        onclick="location.href='Navigate.do?pageType=PRODUCTS&amp;categoryIndex={$categIndex}&amp;subcategoryIndex={$subcategIndex}'"/>                    
                    
                    </td>
                    <td>
                        <input type="submit" value="save"></input>
                    </td>
                </tr>
        </form>
        </table>
                </body>
        </html>


    </xsl:template>
	
</xsl:stylesheet>
	