function checkvalue(form) { 
        
    var productNames = form.elements["productName"];
    var producers =  form.elements["producer"];
    var models = form.elements["model"];
    var datesOfIssue = form.elements["dateOfIssue"];
    var colors = form.elements["color"];
    var prices = form.elements["price"];
    
    var isValid = true;
    var warnings = [];
    
    var productNameRegExp = "[a-zA-Z]+";
    var producerRegExp = "[a-zA-Zа-яА-Я]+";
    var modelRegExp = "[a-zA-Zа-яА-Я]{2}[0-9]{3}";
    var dateOfIssueRegExp = "(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-([1-2][0-9]{3})";
    var colorRegExp = "[a-zA-Z]+";
    var priceRegExp = "(0.(0*[1-9]+|[1-9]+))|([1-9][0-9]*.[0-9]+)";
    
    for (var i = 0; i < productNames.length; i++){
        if(!(productNames[i].value).match(productNameRegExp)) {
            warnings.push("Row "+i+": name " +productNames[i].value+" is not valid."
                +" At least one sybmol is needed");  
            isValid = false;
        }
        if(!(producers[i].value).match(producerRegExp)) {
            warnings.push("Row "+i+": producer " +producers[i].value+" is not valid."
                +" At least one sybmol is needed");
            isValid = false;
        }
        if(!(models[i].value).match(modelRegExp)) {
            warnings.push("Row "+i+": model " +models[i].value+" is not valid."
                +" Two letters, then three numeric. Example: AA123");
            isValid = false;
        }
        if(!(datesOfIssue[i].value).match(dateOfIssueRegExp)) {
             warnings.push("Row "+i+": date " +datesOfIssue[i].value+" is not valid."
                +" (dd-MM-yyyy) Example: 21-01-2001");
            isValid = false;
        }
        if(!(colors[i].value).match(colorRegExp)) {
             warnings.push("Row "+i+": color " +colors[i].value+" is not valid."
                +" At least one sybmol is needed");
            isValid = false;
        }
        if(null != prices && !(prices[i].value).match(priceRegExp)) {
             warnings.push("Row "+i+": price " +prices[i].value+" is not valid."
                +" Example: 1.99");
            isValid = false;
        }
    }
         
    if(false == isValid){
            alert(warnings.join("\n"));
    }  

    return isValid;
}
