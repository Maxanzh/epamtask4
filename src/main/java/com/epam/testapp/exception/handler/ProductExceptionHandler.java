/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.epam.testapp.exception.handler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ExceptionHandler;
import org.apache.struts.config.ExceptionConfig;

/**
 *
 * @author Maxim
 */
public class ProductExceptionHandler extends ExceptionHandler{
 
  private static final Logger LOGGER = Logger.getLogger(ProductExceptionHandler.class);
 
  @Override
  public ActionForward execute(Exception ex, ExceptionConfig ae,
	ActionMapping mapping, ActionForm formInstance,
	HttpServletRequest request, HttpServletResponse response)
	throws ServletException {
      
	LOGGER.error(ex); 
        ex.printStackTrace();
	return super.execute(ex, ae, mapping, formInstance, request, response);
  }
} 
