/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.epam.testapp.exception;

/**
 *
 * @author Maxim_Zhupinsky
 */
public class LogicException extends Exception{
    
    public LogicException(String problem, Exception ex) {
        super(problem,ex);
    }
    
    public LogicException (String problem) {
        super(problem);
    }
    
    public LogicException(Exception ex) {
        super(ex);
    }
}
