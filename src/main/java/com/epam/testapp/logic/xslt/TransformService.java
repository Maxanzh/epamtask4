/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.epam.testapp.logic.xslt;

import com.epam.testapp.exception.LogicException;
import com.epam.testapp.util.ResourceManager;
import com.epam.testapp.util.AppConstants;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;

/**
 *
 * @author Maxim_Zhupinsky
 */
public final class TransformService {
    
    private TransformerFactory transformFactory = TransformerFactory.newInstance(); 
    private Map<TransformationType, Templates> templatesMap = new HashMap<>();
    
    private TransformService() {
    }
    
    public static TransformService getInstance() {
        return TransformServiceHolder.INSTANCE;
    }
    
    private static class TransformServiceHolder {

        private static final TransformService INSTANCE = new TransformService();
    }
    
    public Transformer defineXSLTTransformer(TransformationType type) throws LogicException {
        
        Transformer transformer;
        Templates chosenTemplate;

        try {    
            chosenTemplate = templatesMap.get(type);
            transformer = chosenTemplate.newTransformer();            
        } catch (TransformerConfigurationException ex) {
            throw new LogicException("can't create xslt transformer" , ex);
        }
        return transformer;
    }
        
    public boolean writeFile (File xmlFile, Writer transformResult) 
            throws LogicException {
        
        boolean result =false;
        try{
            Writer fileWriter = new PrintWriter(xmlFile, AppConstants.UTF);
          // System.out.println("file "+transformResult.toString());
            fileWriter.write(transformResult.toString());
            fileWriter.flush();
            result = true;
        } catch (IOException ex) {
            throw new LogicException("can't transform file "+xmlFile.getAbsolutePath(), ex);
        } 
        return result;
    }
        
    public void initialize() throws LogicException {
        
        String xslBundlePath;
        String xslFilePath;
        Source xsltSource;
        Templates template;
        
        try {
            for(TransformationType transformType: TransformationType.values()) {
                xslBundlePath = transformType.defineBundlePath();
                xslFilePath = ResourceManager.getInstance().defineResourcePath(AppConstants.RESOURCE_PATH,
                        xslBundlePath);
                xsltSource = new StreamSource(xslFilePath);
                template = transformFactory.newTemplates(xsltSource);
                templatesMap.put(transformType, template);
            }
        } catch (TransformerConfigurationException ex) {
            throw new LogicException("error initializing xsl template map", ex);        
        }
        }
}
  

