package com.epam.testapp.logic.xslt;

public enum TransformationType {
    
    ADDITIONAL("appResource.filepath.xsl.additional"), 
    SAVE("appResource.filepath.xsl.save");

    private TransformationType(String path) {
       bundlePath = path; 
    }
    // save path to bundle item with real path to xslt file
    private String bundlePath;

    public String defineBundlePath() {
        return bundlePath;
    }
        
}
