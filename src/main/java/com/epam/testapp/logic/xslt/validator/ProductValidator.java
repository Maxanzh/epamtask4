package com.epam.testapp.logic.xslt.validator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import com.epam.testapp.util.AppConstants;
import com.epam.testapp.util.ResourceManager;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Maxim_Zhupinsky
 */
public class ProductValidator {
    
    private Map<String, String> errorMap = new HashMap<>();
    
    private boolean validationFlag = false;
       
    public boolean validateProduct(String productName,
			String producer, String model, String dateOfIssue, 
                        String color, String price) {
        validationFlag = true;
        
        validateField(productName, AppConstants.PRODUCT_REG_EXP, 
                AppConstants.PARAM_PRODUCT_NAME, AppConstants.PRODUCT_ERROR_MESSAGE);
        validateField(producer, AppConstants.PRODUCER_REG_EXP, 
                AppConstants.PARAM_PRODUCER, AppConstants.PRODUCER_ERROR_MESSAGE);        
        validateField(model, AppConstants.MODEL_REG_EXP, 
                AppConstants.PARAM_MODEL, AppConstants.MODEL_ERROR_MESSAGE);
        validateField(dateOfIssue, AppConstants.DATE_REG_EXP, 
                AppConstants.PARAM_DATE_OF_ISSUE, AppConstants.DATE_ERROR_MESSAGE);
        validateField(color, AppConstants.COLOR_REG_EXP, 
                AppConstants.PARAM_COLOR, AppConstants.COLOR_ERROR_MESSAGE);
        validateField(price, AppConstants.PRICE_REG_EXP, 
                AppConstants.PARAM_PRICE, AppConstants.PRICE_ERROR_MESSAGE);
        
        return validationFlag;
    }

    public Map<String, String> getErrorMap() {
        return errorMap;
    }

    public void setErrorMap(Map<String, String> errorMap) {
        this.errorMap = errorMap;
    }

    
    public String getError(String fieldName){
            return errorMap.get(fieldName);
    }
    
    public boolean isValidationFlag() {
        return validationFlag;
    }

    public void setValidationFlag(boolean validationFlag) {
        this.validationFlag = validationFlag;
    }
    
    private void validateField(String fieldValue, String regExpBundle, String paramName, 
            String errorType) {
        
        ResourceManager resourceManager = ResourceManager.getInstance();
        String regExp= resourceManager.getResource(AppConstants.RESOURCE_PATH, regExpBundle);
        String errorMessage= resourceManager.getResource(AppConstants.RESOURCE_PATH, errorType);
        Pattern pattern = Pattern.compile(regExp);
        Matcher matcher = pattern.matcher(fieldValue);
        
        if (!matcher.find()){
            validationFlag =false;
            errorMap.put(paramName, "invalid value "+errorMessage);
        }    
        }
}
