/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.epam.testapp.logic;

import com.epam.testapp.exception.LogicException;
import com.epam.testapp.logic.xslt.validator.ProductValidator;
import com.epam.testapp.util.AppConstants;
import com.epam.testapp.util.ResourceManager;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;


/**
 *
 * @author Maxim_Zhupinsky
 */
public class ProductDocumentService {
    
    private ProductDocumentService() {
    }
    
    public static ProductDocumentService getInstance() {
        return ProductDocumentServiceHolder.INSTANCE;
    }
    
    private static class ProductDocumentServiceHolder {

        private static final ProductDocumentService INSTANCE = new ProductDocumentService();
    }
    
    public Document getXmlDocument() throws LogicException {
        
        ResourceManager resourceManager = ResourceManager.getInstance();
        String xmlFilePath = resourceManager.defineResourcePath(AppConstants.RESOURCE_PATH, 
                AppConstants.XML_FILE_PATH);
        File xmlFile = new File(xmlFilePath);
        SAXBuilder builder = new SAXBuilder();
        Document doc = null;
        
        try{
            doc = builder.build(xmlFile);			
        } catch(JDOMException e){
            throw new LogicException("can't build jdom document",e);
        } catch (IOException ex) {
            throw new LogicException("xml file is not found while building document",ex);
        }
        return doc;
    }
    
    public boolean validateDocument(Document document, int categoryIndex, 
            int subcategoryIndex) {
       
        boolean isValidDocument = true;
        List<Element> products = getProductsFromDocument(document,categoryIndex, subcategoryIndex);
        Iterator<Element> productsIterator = products.iterator();	
        while (productsIterator.hasNext() && isValidDocument) {
            Element productElem = productsIterator.next();
            String productName = productElem.getAttributeValue("name");
            Namespace ns = Namespace.getNamespace(AppConstants.NAMESPACE);
            String producer = productElem.getChildText(AppConstants.PARAM_PRODUCER, ns);
            String model = productElem.getChildText(AppConstants.PARAM_MODEL, ns);
            String dateOfIssue = productElem.getChildText(AppConstants.PARAM_DATE_OF_ISSUE, ns);
            String color = productElem.getChildText(AppConstants.PARAM_COLOR, ns);
            String price = productElem.getChildText(AppConstants.PARAM_PRICE, ns);
            
            // if presents "not in stock tag" set value to past validation
            if (productElem.getChild(AppConstants.PARAM_NOT_IN_STOCK, ns)!= null){
                price = "1.99";
            } 
//            System.out.println("!!!!!!! "+price+productName+producer+model+dateOfIssue+color);
            isValidDocument = new ProductValidator().validateProduct(productName, producer,
                    model, dateOfIssue, color, price);

        }
        return isValidDocument;
    }
    
    public void writeProductToFile(Document newDocument, int categoryIndex, 
            int subcategoryIndex) throws LogicException {
        
        String xmlFilepath = ResourceManager.getInstance().defineResourcePath(AppConstants.RESOURCE_PATH,
            AppConstants.XML_FILE_PATH); 
        XMLOutputter xmlOutput = new XMLOutputter();
	xmlOutput.setFormat(Format.getPrettyFormat());
        Document oldDocument = getXmlDocument();
	// find the products, that can be changed. 
        List<Element> newProducts = getProductsFromDocument(newDocument,categoryIndex, subcategoryIndex);       
        // also find products, that must be rewrote by new products
        List<Element> oldProducts = getProductsFromDocument(oldDocument,categoryIndex, subcategoryIndex);

 
        for(int i = 0; i < newProducts.size(); i++){
            Element newElement = newProducts.get(i);
            Element oldElement = oldProducts.get(i);
            
            Namespace ns = Namespace.getNamespace(AppConstants.NAMESPACE);
            
            String productName = newElement.getAttributeValue("name");
            String producer = newElement.getChildText(AppConstants.PARAM_PRODUCER, ns);
            String model = newElement.getChildText(AppConstants.PARAM_MODEL, ns);
            String dateOfIssue = newElement.getChildText(AppConstants.PARAM_DATE_OF_ISSUE, ns);
            String color = newElement.getChildText(AppConstants.PARAM_COLOR, ns);
            String price = newElement.getChildText(AppConstants.PARAM_PRICE, ns);
            
            oldElement.getAttribute("name").setValue(productName);           
            oldElement.getChild(AppConstants.PARAM_PRODUCER, ns).setText(producer);
            oldElement.getChild(AppConstants.PARAM_MODEL, ns).setText(model);
            oldElement.getChild(AppConstants.PARAM_DATE_OF_ISSUE, ns).setText(dateOfIssue);
            oldElement.getChild(AppConstants.PARAM_COLOR, ns).setText(color);
            
            if (oldElement.getChild(AppConstants.PARAM_NOT_IN_STOCK, ns) == null){
                oldElement.getChild(AppConstants.PARAM_PRICE, ns).setText(price);
            } 
        }

        try {
            xmlOutput.output(oldDocument, new FileWriter(xmlFilepath));
        } catch (IOException ex) {
            throw new LogicException("can't write updated document", ex);
        }
        
    }
    
    private List<Element> getProductsFromDocument(Document document, int categoryIndex, int subcategoryIndex) {
        Element currentCategory = document.getRootElement().getChildren().get(categoryIndex);
        Element currentSubcategory = currentCategory.getChildren().get(subcategoryIndex);
        List<Element> products = currentSubcategory.getChildren();
        return products;
    }
}
