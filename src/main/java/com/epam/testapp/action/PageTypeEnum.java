package com.epam.testapp.action;

enum PageTypeEnum {
    CATEGORIES, SUBCATEGORIES, PRODUCTS
}
