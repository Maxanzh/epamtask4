/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.epam.testapp.action;

import com.epam.testapp.exception.LogicException;
import com.epam.testapp.form.ProductForm;
import com.epam.testapp.logic.ProductDocumentService;
import com.epam.testapp.logic.xslt.TransformService;
import com.epam.testapp.logic.xslt.TransformationType;
import com.epam.testapp.logic.xslt.validator.ProductValidator;
import com.epam.testapp.model.Product;
import com.epam.testapp.util.AppConstants;
import com.epam.testapp.util.ResourceManager;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;
import org.apache.struts.actions.MappingDispatchAction;
import org.jdom2.Document;

/**
 *
 * @author Maxim_Zhupinsky
 */

public final class ProductAction extends MappingDispatchAction {
        
    private final static Logger logger = Logger.getLogger(ProductAction.class);
    private final static ProductDocumentService DOCUMENT_SERVICE = ProductDocumentService.getInstance();
    private final static ReentrantReadWriteLock READ_WRITE_LOCK = new ReentrantReadWriteLock();
    private final static TransformService TRANSFORM_SERVICE = TransformService.getInstance();
        
    public ActionForward navigate (ActionMapping mapping, ActionForm form,
                    HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        logger.info("navigate method started");
        ActionForward redirect;
        ProductForm pForm = (ProductForm) form;            
        
        try {
            READ_WRITE_LOCK.readLock().lock();
            pForm.setCatalog(DOCUMENT_SERVICE.getXmlDocument());
        } finally{
            READ_WRITE_LOCK.readLock().unlock();
        }
        PageTypeEnum pageKey = PageTypeEnum.valueOf(pForm.getPageType());
        
        switch(pageKey){
            case CATEGORIES:
                redirect = mapping.findForward(AppConstants.FORWARD_CATEGORIES);
                break;
            case SUBCATEGORIES:
                redirect = mapping.findForward(AppConstants.FORWARD_SUBCATEGORIES);
                break;
            case PRODUCTS:
                redirect = mapping.findForward(AppConstants.FORWARD_PRODUCTS);
                break;
            default:
                throw new LogicException("can't find back action param");
        }
        return redirect;
    }

    
    public ActionForward goAddPage (ActionMapping mapping, ActionForm form,
                HttpServletRequest request, HttpServletResponse response)
        throws Exception {
        
        logger.info("go add page method started");
        ProductForm pForm = (ProductForm) form;
        ProductValidator validator = new ProductValidator();
        Product product = pForm.getProduct();
        String xmlPath = ResourceManager.getInstance().defineResourcePath(AppConstants.RESOURCE_PATH,
            AppConstants.XML_FILE_PATH);
        
        try {   
            StreamResult outputStream = new StreamResult(response.getWriter());
            Transformer transformer = TRANSFORM_SERVICE.defineXSLTTransformer(TransformationType.ADDITIONAL);
            transformer.setParameter(AppConstants.PARAM_CATEGORY_NAME, product.getCategoryName());
	    transformer.setParameter(AppConstants.PARAM_SUBCATEGORY_NAME, product.getSubcategoryName());
            transformer.setParameter(AppConstants.PARAM_CATEGORY_INDEX, pForm.getCategoryIndex());
	    transformer.setParameter(AppConstants.PARAM_SUBCATEGORY_INDEX, pForm.getSubcategoryIndex());
            transformer.setParameter(AppConstants.PARAM_VALIDATOR, validator);            
            READ_WRITE_LOCK.readLock().lock();
            File xmlFile = new File(xmlPath);
            Source xmlSource = new StreamSource(xmlFile);
            transformer.transform(xmlSource, outputStream);

        }  catch(IOException ex){
            throw new LogicException(ex);
        }  catch (TransformerException e) {
            throw new LogicException("error during add_type transformation");
        } finally {
           READ_WRITE_LOCK.readLock().unlock();
        }
        return null;
    }
    
    public ActionForward saveProduct (ActionMapping mapping, ActionForm form,
                HttpServletRequest request, HttpServletResponse response)
        throws Exception {
        
        long lastModified;
        String xmlPath = ResourceManager.getInstance().defineResourcePath(AppConstants.RESOURCE_PATH,
                AppConstants.XML_FILE_PATH);
        Source xmlSource;
        ActionRedirect actionRedirect = new ActionRedirect(mapping.findForward(AppConstants.FORWARD_PRODUCTS_LIST));
        Writer transformResult = new StringWriter();
        StreamResult outputStream = new StreamResult(transformResult);  
        
        File xmlFile = new File(xmlPath);
        ProductValidator validator = new ProductValidator();        
        Transformer transformer = TRANSFORM_SERVICE.defineXSLTTransformer(TransformationType.SAVE);               
        ProductForm productForm = (ProductForm) form;
        Product product = productForm.getProduct();
        // setting transformer params
        transformer.setParameter(AppConstants.XML_FILE_PATH, xmlPath);
        transformer.setParameter(AppConstants.PARAM_CATEGORY_NAME, product.getCategoryName());
        transformer.setParameter(AppConstants.PARAM_SUBCATEGORY_NAME, product.getSubcategoryName());
        transformer.setParameter(AppConstants.PARAM_PRODUCT_NAME, product.getProductName());
        transformer.setParameter(AppConstants.PARAM_PRODUCER, product.getProducer());
        transformer.setParameter(AppConstants.PARAM_MODEL, product.getModel());
        transformer.setParameter(AppConstants.PARAM_DATE_OF_ISSUE, product.getDateOfIssue());
        transformer.setParameter(AppConstants.PARAM_COLOR, product.getColor());
        transformer.setParameter(AppConstants.PARAM_PRICE, product.getPrice());   
        transformer.setParameter(AppConstants.PARAM_VALIDATOR, validator);
        transformer.setParameter(AppConstants.PARAM_CATEGORY_INDEX, productForm.getCategoryIndex());
        transformer.setParameter(AppConstants.PARAM_SUBCATEGORY_INDEX, productForm.getSubcategoryIndex());
        transformer.setParameter(AppConstants.PARAM_MISSION, "full");

        try {
            READ_WRITE_LOCK.readLock().lock();
            xmlSource = new StreamSource(xmlFile);
            lastModified = xmlFile.lastModified();
            transformer.transform(xmlSource, outputStream);
        } finally {
            READ_WRITE_LOCK.readLock().unlock();
        }

        if (validator.isValidationFlag()) { 
            try {
                READ_WRITE_LOCK.writeLock().lock();
                logger.info("start xml-write process");
                /* if file has been rewrote it must be parsed again, but validation
                 * is not needed, so call just writing part of logic by setting mission='just_write' 
                 */ 
                if(lastModified != xmlFile.lastModified()) {
                    logger.info("document was changed. Repeate document parsing.");
                    transformResult.flush();
                    xmlSource = new StreamSource(xmlFile);						
                    transformer.setParameter(AppConstants.PARAM_MISSION, "just_write");
                    transformer.transform(xmlSource, outputStream);
                }
                TRANSFORM_SERVICE.writeFile(xmlFile,transformResult);
            } finally {
                READ_WRITE_LOCK.writeLock().unlock();
            }

        } else {
            PrintWriter writer = response.getWriter();
            writer.write(transformResult.toString());
            writer.flush();
        }                      
        return actionRedirect;
    }
    
    public ActionForward updateProduct (ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        logger.info("saveChanges method started");
        ActionRedirect actionRedirect = new ActionRedirect(mapping.findForward(AppConstants.FORWARD_PRODUCTS_LIST));
        ProductForm pForm = (ProductForm) form;
        int categoryIndex = pForm.getCategoryIndex();
        int subcategotyIndex = pForm.getSubcategoryIndex();
        Document document = pForm.getCatalog();
        
        boolean isValidationSuccessful = DOCUMENT_SERVICE.validateDocument(document,
                categoryIndex, subcategotyIndex);

        if(isValidationSuccessful){				
            try {
                READ_WRITE_LOCK.writeLock().lock();
                DOCUMENT_SERVICE.writeProductToFile(document, categoryIndex, subcategotyIndex);
                request.setAttribute(AppConstants.PARAM_VALIDATION_MSG,"product is succsessfully updated");
            } finally {
                READ_WRITE_LOCK.writeLock().unlock();
            }
        } else{
            request.setAttribute(AppConstants.PARAM_VALIDATION_MSG,"input values are not valid");
        }
        return actionRedirect;

    }
}
                   
        