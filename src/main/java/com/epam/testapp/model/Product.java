/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.epam.testapp.model;

import java.io.Serializable;

/**
 *
 * @author Maxim_Zhupinsky
 */
public class Product  implements Serializable{
    
    private String categoryName;
    private String subcategoryName;
    private String productName;
    private String producer;
    private String model;
    private String dateOfIssue;
    private String color;
    private String price;

    public String getCategoryName() {
            return categoryName;
    }

    public String getSubcategoryName() {
            return subcategoryName;
    }

    public String getProductName() {
            return productName;
    }

    public String getProducer() {
            return producer;
    }

    public String getModel() {
            return model;
    }

    public String getDateOfIssue() {
            return dateOfIssue;
    }

    public String getColor() {
            return color;
    }

    public String getPrice() {
            return price;
    }

    // ----- Setters -----
    public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
    }

    public void setSubcategoryName(String subcategoryName) {
            this.subcategoryName = subcategoryName;
    }

    public void setProductName(String productName) {
            this.productName = productName;
    }

    public void setProducer(String producer) {
            this.producer = producer;
    }

    public void setModel(String model) {
            this.model = model;
    }

    public void setDateOfIssue(String dateOfIssue) {
            this.dateOfIssue = dateOfIssue;
    }

    public void setColor(String color) {
            this.color = color;
    }

    public void setPrice(String price) {
            this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" + "categoryName=" + categoryName + ", subcategoryName=" + subcategoryName + ", productName=" + productName + ", producer=" + producer + ", model=" + model + ", dateOfIssue=" + dateOfIssue + ", color=" + color + ", price=" + price + '}';
    }

    
}
