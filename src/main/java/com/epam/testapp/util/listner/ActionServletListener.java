/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.epam.testapp.util.listner;

import com.epam.testapp.exception.LogicException;
import com.epam.testapp.logic.xslt.TransformService;
import com.epam.testapp.util.ResourceManager;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.log4j.Logger;

/**
 * Web application lifecycle listener.
 *
 * @author Maxim_Zhupinsky
 */
public class ActionServletListener implements ServletContextListener {

    private static final Logger LOGGER = Logger.getLogger(ActionServletListener.class);
    
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        LOGGER.info("servlet context initialization started");
        String realPath = sce.getServletContext().getRealPath("/");
        ResourceManager.getInstance().setRealPath(realPath);
        try {
            TransformService.getInstance().initialize();
        } catch (LogicException ex) {
            LOGGER.error("can't initialized transform service", ex);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }
}
