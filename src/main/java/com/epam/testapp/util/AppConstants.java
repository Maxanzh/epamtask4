/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.epam.testapp.util;

/**
 *
 * @author Maxim_Zhupinsky
 */
public final class AppConstants {
    
    private AppConstants(){};

    // action forward path
    public static final String FORWARD_CATEGORIES = "categories";
    public static final String FORWARD_SUBCATEGORIES = "subcategories";
    public static final String FORWARD_PRODUCTS = "products";
    public static final String FORWARD_PRODUCTS_LIST = "ProductsList";
    
    
    
    // Pathes to files
    public static final String XML_FILE_PATH = "appResource.filepath.xml";
    public static final String RESOURCE_PATH = "applicationResources";   
    // param constant
    public static final String PARAM_COMMAND = "command"; 
    public static final String PARAM_CATEGORY_NAME = "categoryName";
    public static final String PARAM_SUBCATEGORY_NAME = "subcategoryName";
    public static final String PARAM_NOT_IN_STOCK = "notInStock";
    public static final String PARAM_PRODUCT_NAME = "productName";
    public static final String PARAM_PRODUCER = "producer";
    public static final String NAMESPACE = "http://www.epam.com/catalogXMLSchema";
    public static final String PARAM_MODEL = "model";
    public static final String PARAM_DATE_OF_ISSUE = "dateOfIssue";
    public static final String PARAM_COLOR = "color";
    public static final String PARAM_PRICE = "price";
    public static final String PARAM_VALIDATION_FLAG = "flag";
    public static final String PARAM_MISSION = "mission";
    public static final String PARAM_CREATE_PRODUCT ="createProduct";
    public static final String PARAM_VALIDATION_MSG = "validationMessage";
    public static final String PARAM_XML_PATH = "xmlFilepath";
    public static final String PARAM_CATEGORY_INDEX = "categIndex";
    public static final String PARAM_SUBCATEGORY_INDEX = "subcategIndex";
    
    
    // regExp
    public static final String PRODUCT_REG_EXP="appResource.pattern.product.name";
    public static final String PRODUCER_REG_EXP="appResource.pattern.producer";
    public static final String MODEL_REG_EXP="appResource.pattern.model";
    public static final String DATE_REG_EXP="appResource.pattern.dateOfIssue";
    public static final String COLOR_REG_EXP="appResource.pattern.color";
    public static final String PRICE_REG_EXP="appResource.pattern.price";
    //messages
    public static final String PRODUCT_ERROR_MESSAGE="appResource.error.name";
    public static final String PRODUCER_ERROR_MESSAGE="appResource.error.producer";
    public static final String MODEL_ERROR_MESSAGE="appResource.error.model";
    public static final String DATE_ERROR_MESSAGE="appResource.error.date";
    public static final String COLOR_ERROR_MESSAGE="appResource.error.color";
    public static final String PRICE_ERROR_MESSAGE="appResource.error.price";
    public static final String PARAM_UDPATE_MESSAGE = "updateMessage";

    public static final String PARAM_VALIDATOR = "validator";
    public static final String FORWARD_PRODUCT_LIST = "controller?command=view_products";
    public static final String CATEGORY_ID_PARAM = "&categoryName=";
    public static final String SUBCATEGORY_ID_PARAM = "&subcategoryName=";
    public static final String UTF = "UTF-8";
    public static final String EMTY_STRING = "";

}
