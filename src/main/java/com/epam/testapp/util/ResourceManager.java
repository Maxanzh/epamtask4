/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.epam.testapp.util;

import com.epam.testapp.logic.ProductDocumentService;
import java.util.ResourceBundle;

/**
 *
 * @author Maxim_Zhupinsky
 */
public final class ResourceManager {
    
    private static String realPath;
    
    private ResourceManager() {
    }
    
    public static ResourceManager getInstance() {
        return ResourceManagerHolder.INSTANCE;
    }
    
    private static class ResourceManagerHolder {

        private static final ResourceManager INSTANCE = new ResourceManager();
    }
    
    public String getResource(String bundlePath, String resourcePath) {
        ResourceBundle bundle = ResourceBundle.getBundle(bundlePath);       
        return bundle.getString(resourcePath);         
    }
    
    public String defineResourcePath(String bundlePath, String resourcePath) {
       ResourceBundle bundle = ResourceBundle.getBundle(bundlePath);   
       return realPath+bundle.getString(resourcePath);
    }
    
    public String getRealPath() {
        return realPath;
    }

    public void setRealPath(String realPath) {
        ResourceManager.realPath = realPath;
    }
}
