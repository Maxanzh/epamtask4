package com.epam.testapp.util.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public final class CharsetFilter implements Filter {
	public final static String ENCODING_UTF8 = "UTF-8";

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

	/**
	 * This method checks the request encoding and sets an UTF-8 encoding.
	 * 
	 * @throws IOException
	 * @throws ServletException
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain filterChain) throws IOException, ServletException {
		String encoding = request.getCharacterEncoding();
		if (!ENCODING_UTF8.equals(encoding)) {
			request.setCharacterEncoding(ENCODING_UTF8);
		}
		filterChain.doFilter(request, response);

	}

	@Override
	public void destroy() {
	}

}
