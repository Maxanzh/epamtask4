/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.epam.testapp.form;

import com.epam.testapp.model.Product;
import org.apache.struts.action.ActionForm;
import org.jdom2.Document;

/**
 *
 * @author Maxim_Zhupinsky
 */
public class ProductForm extends ActionForm {
    
    private Document catalog;
    private Product product = new Product();
    private String pageType;      // type of page to  navigate after the action          
    private int categoryIndex;
    private int subcategoryIndex;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Document getCatalog() {
        return catalog;
    }

    public void setCatalog(Document catalog) {
        this.catalog = catalog;
    }

    public int getCategoryIndex() {
        return categoryIndex;
    }

    public void setCategoryIndex(int categoryIndex) {
        this.categoryIndex = categoryIndex;
    }

    public int getSubcategoryIndex() {
        return subcategoryIndex;
    }

    public void setSubcategoryIndex(int subcategoryIndex) {
        this.subcategoryIndex = subcategoryIndex;
    }
    
    public String getPageType() {
        return pageType;
    }

    public void setPageType(String pageType) {
        this.pageType = pageType;
    }
    
    
    public String getCategoryName() {
            return product.getCategoryName();
    }

    public String getSubcategoryName() {
            return product.getSubcategoryName();
    }

    public String getProductName() {
            return product.getProductName();
    }

    public String getProducer() {
            return product.getProducer();
    }

    public String getModel() {
            return product.getModel();
    }

    public String getDateOfIssue() {
            return product.getDateOfIssue();
    }

    public String getColor() {
            return product.getColor();
    }

    public String getPrice() {
            return product.getPrice();
    }

    public void setCategoryName(String categoryName) {
            this.product.setCategoryName(categoryName);
    }

    public void setSubcategoryName(String subcategoryName) {
            this.product.setSubcategoryName(subcategoryName);
    }

    public void setProductName(String productName) {
            this.product.setProductName(productName);
    }

    public void setProducer(String producer) {
            this.product.setProducer(producer);
    }

    public void setModel(String model) {
            this.product.setModel(model);
    }

    public void setDateOfIssue(String dateOfIssue) {
            this.product.setDateOfIssue(dateOfIssue);
    }

    public void setColor(String color) {
            this.product.setColor(color);
    }

    public void setPrice(String price) {
            this.product.setPrice(price);
    }

      
    
}
